# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is created for ECS task to create simple API for a CAR product. There are add, list and list by id end points. Data is not persisted.

### How do I get set up? ###

There are 2 ways to set up and access this application. 
I strongly suggest to use second option (Docker image) as it is easy to pull and run the image.

Pre-requisites
a. JRE/JDK 11.0 or above.
b. mvn 
c. Postman client
d. Docker desktop service

1. Source code and mvn build
    Download (clone) the repository into local system. Local system, it is expected to have JDK/JRE (11 or above) and Maven installed already.
    Change to the repository cloned from bitbucket. Compile the source with command "mvn clean install -D skipTests"
    Now run application with command java -jar target\cardemo-1.0.0.jar


2. Accessing application by using Docker Images.
   This is the easy way to run this 
	Docker images are created and pushed to Dockerhub. Image can be pulled and run in Docker engine which can be accessed via postman as mentioned above.
    In this repo there is docker-compose.yml.
    Download this file and run command line from the location where it is downloaed 
    docker-compose up -d
    
Swagger Documentation:
  Once service is up and running api documentation can be accessed from http://localhost:8080/swagger-ui.html
  

### Contribution guidelines ###

Units tests need to be completed and will be done in due course.

### Who do I talk to? ###

asesha@yahoo.com