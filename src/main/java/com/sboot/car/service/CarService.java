package com.sboot.car.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sboot.car.domain.Car;
import com.sboot.car.repository.CarRepository;

@Service
public class CarService {
	
	@Autowired
	private CarRepository carRepo;
	
	public List<Car> listAll() {
		return  (List<Car>) carRepo.findAll();
	}
	
	public Optional<Car> getCar(Long id) {
		return carRepo.findById(id);
	}
	
	public Car addCar(Car car) {
		
		return carRepo.save(car);
	}

}
