package com.sboot.car.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CARS")
public class Car {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
	private String make;
	private String model;
	private String colour;
	private int year;
	

	public Car() {
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		if (null == id)
		{ return 0;}
		else {
			return id;}
	}
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public Car(Long id, String make, String model, String colour, int year) {
		super();
		this.id = id;
		this.make = make;
		this.model = model;
		this.colour = colour;
		this.year = year;
	}
	


}
