package com.sboot.car.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sboot.car.domain.Car;

@Repository
public interface CarRepository extends CrudRepository<Car, Long > {

}
