package com.sboot.car.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sboot.car.domain.Car;
import com.sboot.car.service.CarService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
public class CarController {

	@Autowired
	private CarService carService;
	
	@GetMapping("/cars/{id}") 
	@ApiOperation(value = "Find Car by Id",
			notes = "Provide a Car ID to lookup for specific Car from Car List",
			response = Car.class)
	public ResponseEntity<?> getCar(@ApiParam(value = "ID value for the Car that need to be retrieved", required = true)@PathVariable long id)  {

		return new ResponseEntity<>(carService.getCar(id), HttpStatus.OK);
	}
	
	@GetMapping("/cars/all")
	@ApiOperation(value = "Show all cars ",
		notes = "Shows All available Cars",
		response = Car.class)
	public ResponseEntity<?>  getAllCars(){
	
		return new ResponseEntity<>(carService.listAll(),HttpStatus.OK);
	}
	
	@PostMapping("/cars/add")
	@ApiOperation(value = "Add a car to the List ",
		notes = "Adds new car to the list with given details",
		response = Car.class)
	public Car addCar(@RequestBody Car car){
		carService.addCar(car);
		return car;
		
	}
}
